import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:testing_dispose_sample/main.dart' as app;

void main(){
    IntegrationTestWidgetsFlutterBinding.ensureInitialized();

    testWidgets('counter test', (WidgetTester tester) async {
      app.main();
      await tester.pumpAndSettle();
      final Finder screenOneButton = find.byKey(const Key('screen_one_text_button'));
      final Finder screenOneFloatingButton = find.byKey(const Key('screen_one_floating_button'));
      final Finder screenTwoTitle = find.byKey(const Key('screen_two_title'));
      final Finder screenTwoCounter = find.byKey(const Key('screen_two_counter'));

      await tester.tap(screenOneFloatingButton);
      await tester.tap(screenOneButton);

      await tester.pumpAndSettle();

      expect(screenTwoTitle, findsOneWidget);
    });

}