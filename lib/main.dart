import 'package:flutter/material.dart';
import 'package:testing_dispose_sample/routing/routes.dart';
import 'routing/route_generator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: ScreenOne,
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}

