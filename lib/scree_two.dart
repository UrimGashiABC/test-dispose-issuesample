import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'providers/counter_provider.dart';

class ScreenTwoPage extends StatefulWidget {
  @override
  _ScreenTwoPageState createState() => _ScreenTwoPageState();
}

class _ScreenTwoPageState extends State<ScreenTwoPage> {
  @override
  Widget build(BuildContext context) {
    CounterProvider counterProvider = Provider.of<CounterProvider>(
      context,
      listen: false,
    );
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Screen #2',
                key: const Key('screen_two_title'),
              ),
              SizedBox(
                height: 150,
              ),
              Text(
                'Counter: ${counterProvider.counter}',
                key: const Key('screen_two_counter'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
