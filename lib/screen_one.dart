import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testing_dispose_sample/providers/counter_provider.dart';
import 'package:testing_dispose_sample/routing/routes.dart';

class ScreenOnePage extends StatefulWidget {
  @override
  _ScreenOnePageState createState() => _ScreenOnePageState();
}

class _ScreenOnePageState extends State<ScreenOnePage> {

  @override
  Widget build(BuildContext context) {
    CounterProvider counterProvider = Provider.of<CounterProvider>(
      context,
      listen: true,
    );
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Screen #1',
            ),
            SizedBox(
              height: 150,
            ),
            Text(
              '${counterProvider.counter}',
              style: Theme.of(context).textTheme.headline4,
            ),
            SizedBox(
              height: 150,
            ),
            TextButton(
              key: const Key('screen_one_text_button'),
              onPressed: () {
                Navigator.of(context).pushNamed(ScreenTwo, arguments: counterProvider);
              },
              child: Text('Go to screen#2'),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        key: const Key('screen_one_floating_button'),
        onPressed: () => counterProvider.increaseCounter(),
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), //
    );
  }
}
