import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testing_dispose_sample/providers/counter_provider.dart';
import 'package:testing_dispose_sample/scree_two.dart';
import 'package:testing_dispose_sample/screen_one.dart';
import 'package:testing_dispose_sample/utils/navigation_animations.dart';

import 'routes.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case ScreenOne:
        return SlideAnimationTween(
            widget: ChangeNotifierProvider<CounterProvider>(
          create: (_) => CounterProvider(),
          lazy: false,
          child: ScreenOnePage(),
        ));
      case ScreenTwo:
        return SlideAnimationTween(
          widget: ChangeNotifierProvider<CounterProvider>.value(
            value: settings.arguments as CounterProvider,
            child: ScreenTwoPage(),
          ),
        );
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute<void>(builder: (_) {
      return Scaffold(
        appBar: AppBar(title: const Text('Error')),
        body: Center(
          child: Container(
            child: const Text('Error Screen'),
          ),
        ),
      );
    });
  }
}
